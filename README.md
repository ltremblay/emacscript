# Emacscript

What we have here is essentially Parenscript for Emacs.

## Compatibility

Providing transparent use of the same code for Parenscript in Common Lisp and also Emacscript in Emacs Lisp.

## Integration

Convenient functionality for using Emacscript in place of JavaScript:

* elnode
> `(elnode-start)...`

* org-mode
> * Inline code blocks
> * Generating HTML

* skewer

## eslm

Emacsript REPL like emacs `M-x ielm`

# See Also

https://github.com/johnmastro/trident-mode.el